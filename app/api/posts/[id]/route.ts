import { NextResponse } from "next/server";
import { redirect } from "next/navigation";
import { cookies, headers } from "next/dist/client/components/headers";

// получение параметров URL
export async function DELETE(
    req: Request,
    { params }: { params: { id: string } }
  ) {
    const id = params?.id;

    const headerList = headers();
    const type = headerList.get('Content-type');

    const cookieList = cookies();
    const coo1 = cookieList.get('Cookie_1')?.value;
  
    // some logic for delete post by id
    //redirect('/blog');
  
    return NextResponse.json({ id, type, coo1 });
  }