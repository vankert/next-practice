import { authConfig } from "@/configs/auth";
import { Metadata } from "next"
import {getServerSession} from "next-auth/next"
import Image from 'next/image'


export const metadata: Metadata = {
    title: 'Profile',
  }

export default async function Profile() {
    const session = await getServerSession(authConfig);


    return (
        <div>
            <h3>Profile of {session?.user?.name}</h3>
            {session?.user?.image && <Image src={session.user.image} alt="avatar" width={200} height={200} />}
            
        </div>
    )
}