import GoogleButton from "@/components/GoogleButton/GoogleButton";
import SignInForm from "@/components/SignInForm/SignInForm";

export default async function SignIn() {
    return (
        <>
            <h1>SignIn</h1>
            <GoogleButton/>
            <div>or</div>
            <SignInForm/>
        </>
    )
}