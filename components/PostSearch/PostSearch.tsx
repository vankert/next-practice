'use client';

import { getPostsBySearch } from "@/services/getPosts";
import { ChangeEvent, FormEventHandler, useState } from "react";

type Props = {
    onSearch: (value: any[]) => void
}

function PostSearch({onSearch}: Props) {
    const [search, setSearch] = useState('');

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value);
    };

    const handleSubmit: FormEventHandler<HTMLFormElement> = async (event) => {
        event.preventDefault();

        const posts = await getPostsBySearch(search);

        onSearch(posts);
    }

    return (
        <form onSubmit={handleSubmit}>
            <input 
                type="search" 
                placeholder="search" 
                value={search} 
                onChange={handleChange} />
            <button type="submit">Search</button>
        </form>
    )
}

export default PostSearch;