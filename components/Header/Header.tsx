import "./header.css"
import Nav from "../Nav/Nav";

const navLinks = [
    {label: "Home", href: "/"},
    {label: "Blog", href: "/blog"},
    {label: "About", href: "/about"}
]


function Header() {
    return (
        <header className="header">
            <div className="container">
                <Nav navLinks = {navLinks}/>
            </div>
        </header>
    )
}

export default Header;