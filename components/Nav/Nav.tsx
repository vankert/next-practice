'use client';

import "./nav.css"
import Link from "next/link";
import {usePathname} from "next/navigation";
import {useSession, signOut} from "next-auth/react"

type NavLink = {
    label: string;
    href: string;
}

type Props = {
    navLinks: NavLink[];
}

function Nav({navLinks}: Props) {
    const pathname = usePathname();
    const session = useSession();
    

    return (
        <nav>
            {
                navLinks.map(link => {
                    const isActive = pathname === link.href;

                    return (
                        <Link key={link.label} href={link.href} className={isActive ? "active" : ""}>
                            {link.label}
                        </Link>
                    )
                })
            }

            {session?.data && (
                <Link href="/profile" className={pathname === '/profile' ? "active" : ""}>Profile</Link>
            )}
            {session?.data ? <Link href="#" onClick={() => {signOut({callbackUrl: "/"})}}>Sign Out</Link> : <Link href="/signin" className={pathname === '/signin' ? "active" : ""}>Sign In</Link>}
        </nav>
    )
}

export default Nav;